/*


// First step:
// https://usr.minjust.gov.ua/content/free-search

// Then

// Get recaptcha iframe with src parameter token (k - recaptcha token):
// $('iframe[title="reCAPTCHA"]').src
// https://www.google.com/recaptcha/api2/anchor?ar=1&k=6LdStXoUAAAAAE2oEyZLHgu3dBE-WV1zOvZon7_v&co=aHR0cHM6Ly91c3IubWluanVzdC5nb3YudWE6NDQz&hl=uk&v=dpzVjBAupwRfx3UzvXRnnAKb&size=normal&cb=stee32jnhkeo

// Get token from recaptcha src (url)

Send API call to the: https://api.capmonster.cloud/createTask/

Wait 300ms-6000ms. Repeat sending request while not get response
https://api.capmonster.cloud/getTaskResult/

Search request for company:
https://usr.minjust.gov.ua/USRWebAPI/api/public/search?company=33426253&c={RECAPTCHA_V2_TOKEN}

Here we get the data:
https://usr.minjust.gov.ua/USRWebAPI/api/public/detail?rfId={ESCAPED_rfId_TOKEN}

Store data to the `output/parser-data.txt`

Invoke excel file updater

Repeat cycle...

*/

const fs = require('fs');
const process = require("child_process");
const Casper = require('casper');

function loadEnterpriseCodes() {
    const codes = [];
    const f = fs.open(sourceFilename, 'r');

    do {
        var code = f.readLine();
        if (code) codes.push(code);
    } while (code);

    f.close();

    return codes;
}

function createRecaptchaTask() {
    console.log('adwada');
    process.execFile('node', ['source/capmonster/create-task.js'], function (err, stdout, stderr) {
        console.log("execFileError:", JSON.stringify(err))
        console.log("execFileSTDOUT:", JSON.stringify(stdout))
        console.log("execFileSTDERR:", JSON.stringify(stderr))
    });
    console.log('adwada');
}

function getRecaptchaTaskResult() {

}

function getRecaptchaCode() {

}

function storeEnterpriseData(filename, code, parsedData) {
    const data = {
        code: parsedData
    };

    fs.write(filename, JSON.stringify(data) + "\n", 'w');
}

// ---------------------------------------------------------------------------------------------

// Casper instance
const casper = Casper.create({
    verbose: true,
    logLevel: 'debug',
    userAgent: 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36'
});

const capmonsterToken = casper.cli.options['capmonster_token'];
const recaptchaPublicKey = '6LdStXoUAAAAAE2oEyZLHgu3dBE-WV1zOvZon7_v';
const sourceFilename = casper.cli.has('source') ? casper.cli.get('source') : 'input/list.txt';
const distFilename = casper.cli.has('dist') ? casper.cli.get('dist') : 'output/parser-data.txt';

const enterpriseCodes = loadEnterpriseCodes();

// ---------------------------------------------------------------------------------------------

createRecaptchaTask();

// casper.start();

// casper.each(enterpriseCodes, function (self, enterpriseCode) {
//     console.log('enterpriseCode = ' + enterpriseCode);

//     createRecaptchaTask();
// });

// casper.run();

const ExcelJS = require('exceljs');
const fs = require('fs');

const sourceFile = 'input/list.xlsx';
const distFile = 'input/list.txt';

const workbookReader = new ExcelJS.stream.xlsx.WorkbookReader(sourceFile, {
    sharedStrings: 'emit',
    hyperlinks: 'emit',
    worksheets: 'emit',
});

workbookReader.on('worksheet', worksheet => {
    worksheet.on('row', row => {
        let code = row.getCell(3).value;

        if (code && typeof code == 'number') {
            console.log(code + "; " + typeof code);

            code = code.toString() + "\n";

            fs.appendFile(distFile, code, (err) => {
                if (err) {
                    console.error(err);
                }
            });
        }
    });
});

workbookReader.on('shared-strings', sharedString => {
// ...
});

workbookReader.on('hyperlinks', hyperlinksReader => {
// ...
});

workbookReader.on('end', () => {
    console.log('Finish reading sheet file');
});
workbookReader.on('error', (err) => {
    console.error(err);
});

workbookReader.read();

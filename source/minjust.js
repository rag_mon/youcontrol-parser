/*


// First step:
// https://usr.minjust.gov.ua/content/free-search

// Then

// Get recaptcha iframe with src parameter token (k - recaptcha token):
// $('iframe[title="reCAPTCHA"]').src
// https://www.google.com/recaptcha/api2/anchor?ar=1&k=6LdStXoUAAAAAE2oEyZLHgu3dBE-WV1zOvZon7_v&co=aHR0cHM6Ly91c3IubWluanVzdC5nb3YudWE6NDQz&hl=uk&v=dpzVjBAupwRfx3UzvXRnnAKb&size=normal&cb=stee32jnhkeo

// Get token from recaptcha src (url)

Send API call to the: https://api.capmonster.cloud/createTask/

Wait 300ms-6000ms. Repeat sending request while not get response
https://api.capmonster.cloud/getTaskResult/

Search request for company:
https://usr.minjust.gov.ua/USRWebAPI/api/public/search?company=33426253&c={RECAPTCHA_V2_TOKEN}

Here we get the data:
https://usr.minjust.gov.ua/USRWebAPI/api/public/detail?rfId={ESCAPED_rfId_TOKEN}

Store data to the `output/parser-data.txt`

Invoke excel file updater

Repeat cycle...

*/



const Axios = require('axios');
const ExcelJS = require('exceljs');
const PromisePool = require('es6-promise-pool')

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function capmonsterCreateTask(capmonsterToken) {
    const data = {
        clientKey: capmonsterToken,
        task: {
            type: 'NoCaptchaTaskProxyless',
            websiteURL: 'https://usr.minjust.gov.ua/content/free-search',
            websiteKey: '6LdStXoUAAAAAE2oEyZLHgu3dBE-WV1zOvZon7_v'
        }
    };

    const response = await axios.post('https://api.capmonster.cloud/createTask', data);

    if (!response.data.errorId) {
        return response.data.taskId;
    } else {
        throw new Error(response.data);
    }
}

async function capmonsterGetTaskResult(capmonsterToken, taskId, retries, delay) {
    retries = retries || 20;
    delay = delay || 1000;

    const data = {
        "clientKey": capmonsterToken,
        "taskId": taskId
    };

    console.debug('capmonsterGetTaskResult', capmonsterToken, taskId, retries, delay, data);

    let response = null;
    for (let i = 1; i <= retries; i++) {
        response = await axios.post('https://api.capmonster.cloud/getTaskResult/', data);

        console.log('try #' + i);
        console.log('response from https://api.capmonster.cloud/getTaskResult', response.data);

        // without errors
        if (!response.data.errorId) {
            if (response.data.status == 'ready') {
                return response.data.solution.gRecaptchaResponse;
            }

            if (response.data.status == 'processing') {
                await sleep(delay);
                continue;
            }
        } else {
            throw new Error(response.data);
        }
    }

    // Tries limit reached
    throw new Error('Tries limit reached ' + retries);
}

async function searchEnterpriseRFID(enterpriseCode, recaptchaToken) {
    const url = 'https://usr.minjust.gov.ua/USRWebAPI/api/public/search?company=' + encodeURIComponent(enterpriseCode) 
                    + '&c=' + encodeURIComponent(recaptchaToken);
    
    const response = await axios.get(url);

    const rfId = response.data.searchData[0].rfId;

    if (rfId) {
        return rfId;
    } else {
        console.debug("Can't find enterprise rfId in response: ", response.data);

        throw new Error('Enterprise not found by code #' + enterpriseCode);
    }
}

function prepareApiData(apiData) {
    const phoneRegex = /(\+?)[\-\(\)\d]{9,}/gm;

    const data = {};

    for (const row of apiData) {
        // phone
        if (row.orderNum == 30) {
            const match = row['value'].match(phoneRegex);
            data['phone'] = match ? match[0] : '-';
        }
    }

    return data;
}

async function getEnterprisePageData(enterpriseRFID) {
    const url = 'https://usr.minjust.gov.ua/USRWebAPI/api/public/detail?rfId=' + encodeURIComponent(enterpriseRFID);

    const response = await axios.get(url);

    const data = prepareApiData(response.data);

    return data;
}

async function getEnterpriseData(capmonsterToken, enterpriseCode) {
    const capmonsterTaskId = await capmonsterCreateTask(capmonsterToken);
    
    console.debug('Created capmonster task with ID #' + capmonsterTaskId);

    const recaptchaToken = await capmonsterGetTaskResult(capmonsterToken, capmonsterTaskId);

    await sleep(5000);

    console.debug('Received ReCaptcha token: ' + recaptchaToken);

    const enterpriseRFID = await searchEnterpriseRFID(enterpriseCode, recaptchaToken);

    console.debug('Enterprise rfID: ', enterpriseRFID);

    return await getEnterprisePageData(enterpriseRFID);
}

// ------------------------------------------------------------------------------------------------------

// function getRandomArbitrary(min, max) {
//     return Math.random() * (max - min) + min;
// }






// common
const capmonsterToken = '258b965dfc46d40502c59f4c3cc08174';
const inputFilename = 'input/list.xlsx';
const destFilename = 'output/updated-list.xlsx';

// Excel
const CELL_CODE = 3;
const CELL_PHONE = 7;

// Promise pool configs
let promiseCount = 270;
const rowsPerPromise = 5;
const concurrency = 1;

const workbook = new ExcelJS.Workbook();
const axios = Axios.create();
axios.defaults.timeout = 10000;

async function parse(workbook, rowStart, rowEnd, promiseIndex) {
    // console.debug('Loading xlsx file: ' + inputFilename);
    // await workbook.xlsx.readFile(inputFilename);

    const worksheet = workbook.getWorksheet('готово');
    const rowsCount = worksheet.actualRowCount;

    rowStart = rowStart || 0;
    rowEnd = rowEnd || rowsCount;

    console.debug(`rowStart = ${rowStart}; rowEnd = ${rowEnd}`);

    for (let rowNum = rowStart; rowNum <= rowEnd; rowNum++) {
        console.debug('Processing row #' + rowNum);

        const row = worksheet.getRow(rowNum);

        const enterpriseCode = row.getCell(CELL_CODE).value;

        // It's not exterprise row
        if (typeof enterpriseCode != 'number')
            continue;

        // Enterprise telephone already set
        if (row.getCell(CELL_PHONE).value)
            continue;

        console.log('Processing enterprise by code #' + enterpriseCode);

        try {
            const enterpriseData = await getEnterpriseData(capmonsterToken, enterpriseCode);

            console.log('Scraped enterprise data:', enterpriseData);

            // updating row columns
            row.getCell(CELL_PHONE).value = enterpriseData.phone;

            // row.commit();

        } catch (e) {
            console.debug('Thrown error when scraping enterprise data', e);
            console.info(`Skiping row #${rowNum}`);

            continue;
        }
    }

    // Save backup
    await workbook.xlsx.writeFile(`output/backup-${rowStart}_${rowEnd}.xlsx`);

    return promiseIndex;
}


// ---------------------------------------------------------------------------------------

/*

    Rinning application

*/


// For first load xlsx file
console.debug('Loading xlsx file: ' + inputFilename);
workbook.xlsx.readFile(inputFilename)
    .then((workbook) => {
        console.debug('Loaded xlsx file: ' + inputFilename);

        const promiseProducer = function () {
            if (promiseCount < 13000) {
                promiseCount++;
        
                const rowEnd = (promiseCount * rowsPerPromise) - 1;
                const rowStart = (promiseCount * rowsPerPromise) - rowsPerPromise;
                const promiseIndex = promiseCount;
        
                return parse(workbook, rowStart, rowEnd, promiseIndex);
            } else
                return null;
        };

        const pool = new PromisePool(promiseProducer, concurrency);

        pool.addEventListener('fulfilled', function (event) {
            // The event contains:
            // - target:    the PromisePool itself
            // - data:
            //   - promise: the Promise that got fulfilled
            //   - result:  the result of that Promise
            console.log('Fulfilled: ' + event.data.result)
        })
        
        pool.addEventListener('rejected', function (event) {
            // The event contains:
            // - target:    the PromisePool itself
            // - data:
            //   - promise: the Promise that got rejected
            //   - error:   the Error for the rejection
            console.log('Rejected: ' + event.data.error.message)
        })

        pool.start()
            .then(() => {
                console.log('Promise pool finished');
            })
            .finally(() => {
                console.log('Finally');

                workbook.xlsx.writeFile(destFilename);
            });
    });




// const ProxyLists = require('proxy-lists');

// ProxyLists.getProxies({
// 	// options
// 	countries: ['us', 'ca']
// })
// 	.on('data', function(proxies) {
// 		// Received some proxies.
// 		console.log('got some proxies');
// 		console.log(proxies);
// 	})
// 	.on('error', function(error) {
// 		// Some error has occurred.
// 		console.log('error!', error);
// 	})
// 	.once('end', function() {
// 		// Done getting proxies.
// 		console.log('end!');
// 	});

// // `getProxiesFromSource` returns an event emitter.
// ProxyLists.getProxiesFromSource('freeproxylists', {
// 	anonymityLevels: ['elite']
// })
// 	.on('data', function(proxies) {
// 		// Received some proxies.
// 		console.log('got some proxies');
// 		console.log(proxies);
// 	})
// 	.on('error', function(error) {
// 		// Some error has occurred.
// 		console.log('error!', error);
// 	})
// 	.once('end', function() {
// 		// Done getting proxies.
// 		console.log('end!');
// 	});





// const fetch = require('node-fetch');
// const HttpsProxyAgent = require('https-proxy-agent');


// (async () => {
//     const proxyAgent = new HttpsProxyAgent('1.70.66.15:9999');
//     const response = await fetch('https://httpbin.org/ip?json', { agent: proxyAgent});
//     const body = await response.text();
//     console.log(body);
// })();




// var unirest = require("unirest");

// var req = unirest("GET", "https://rotating-proxy-api.p.rapidapi.com/");

// req.query({
// 	"apiKey": "SzLVqAHQsERFNGkhwKtDUZefyn6xC5m4"
// });

// req.headers({
// 	"x-rapidapi-key": "02e672673bmshd8b651c96496435p18388bjsn87009844c6b1",
// 	"x-rapidapi-host": "rotating-proxy-api.p.rapidapi.com",
// 	"useQueryString": true
// });


// req.end(function (res) {
// 	if (res.error) throw new Error(res.error);

// 	console.log(res.body);
// });









// const fetch = require('node-fetch');
// const HttpsProxyAgent = require('https-proxy-agent');


// (async () => {
//     const proxyAgent = new HttpsProxyAgent('http://209.45.3.233:999');
//     const response = await fetch('https://httpbin.org/ip?json', { agent: proxyAgent});
//     const body = await response.text();
//     console.log(body);
// })();



// // Test request call proxy
// var httpsProxyAgent = require("https-proxy-agent");
// var agent = new httpsProxyAgent("http://209.45.3.233:999");
// axios.get('https://httpbin.org/ip?json', {
//         // `proxy` means the request actually goes to the server listening
//         // on localhost:3000, but the request says it is meant for
//         // 'http://httpbin.org/get?answer=42'
//         // proxy: {
//         //     host: '209.45.3.233',
//         //     port: 999,
//         // }
//         httpsAgent: agent
//     })
//     .then((response) => {
//         console.log(response);
//     });


/*


data: {
    proxy: '45.169.16.22:8080',
    ip: '45.169.16.22',
    port: '8080',
    connectionType: 'Residential',
    asn: '268087',
    isp: 'RADIONET TELECOM LTDA',
    type: 'elite',
    lastChecked: 1619431553,
    get: true,
    post: true,
    cookies: true,
    referer: true,
    userAgent: true,
    city: 'unknown',
    state: 'TO',
    country: 'BR',
    randomUserAgent: 'Mozilla/5.0 (Linux U Android 3.0 en-us Xoom Build/HRI39) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13',
    requestsRemaining: 49
  }


  data: {
    proxy: '160.19.155.51:8080',
    ip: '160.19.155.51',
    port: '8080',
    connectionType: 'Mobile',
    asn: '328236',
    isp: 'PCS-Holdings-Limited',
    type: 'elite',
    lastChecked: 1619427532,
    get: true,
    post: true,
    cookies: true,
    referer: true,
    userAgent: true,
    city: 'Freetown',
    state: 'W',
    country: 'SL',
    randomUserAgent: 'Mozilla/5.0 (Windows U Win98 de-DE rv:0.9.4.1) Gecko/20020314 Netscape6/6.2.2',
    requestsRemaining: 48
  }


  data: {
    proxy: '111.72.195.138:3256',
    ip: '111.72.195.138',
    port: '3256',
    connectionType: 'Residential',
    asn: '4134',
    isp: 'China Telecom',
    type: 'elite',
    lastChecked: 1619431134,
    get: true,
    post: true,
    cookies: true,
    referer: true,
    userAgent: true,
    city: 'Beijing',
    state: 'BJ',
    country: 'CN',
    randomUserAgent: 'Mozilla/5.0 (Linux Android 4.1.2 Xoom Build/JZO54K) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Safari/535.19',
    requestsRemaining: 47
  }


data: {
    proxy: '114.99.13.57:3256',
    ip: '114.99.13.57',
    port: '3256',
    connectionType: 'Residential',
    asn: '4134',
    isp: 'China Telecom',
    type: 'elite',
    lastChecked: 1619439833,
    get: true,
    post: true,
    cookies: true,
    referer: true,
    userAgent: true,
    city: 'Hefei',
    state: 'AH',
    country: 'CN',
    randomUserAgent: 'Mozilla/5.0 (compatible MSIE 10.0 Windows NT 6.2 Win64 x64 Trident/6.0)',
    requestsRemaining: 46
  }



*/


// // Proxy rotator
// axios.get('http://falcon.proxyrotator.com:51337/?apiKey=SzLVqAHQsERFNGkhwKtDUZefyn6xC5m4').then((response) => {
//     console.log(response.data);
// });
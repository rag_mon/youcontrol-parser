/*


// First step:
// https://usr.minjust.gov.ua/content/free-search

// Then

// Get recaptcha iframe with src parameter token (k - recaptcha token):
// $('iframe[title="reCAPTCHA"]').src
// https://www.google.com/recaptcha/api2/anchor?ar=1&k=6LdStXoUAAAAAE2oEyZLHgu3dBE-WV1zOvZon7_v&co=aHR0cHM6Ly91c3IubWluanVzdC5nb3YudWE6NDQz&hl=uk&v=dpzVjBAupwRfx3UzvXRnnAKb&size=normal&cb=stee32jnhkeo

// Get token from recaptcha src (url)

Send API call to the: https://api.capmonster.cloud/createTask/

Wait 300ms-6000ms. Repeat sending request while not get response
https://api.capmonster.cloud/getTaskResult/

Search request for company:
https://usr.minjust.gov.ua/USRWebAPI/api/public/search?company=33426253&c={RECAPTCHA_V2_TOKEN}

Here we get the data:
https://usr.minjust.gov.ua/USRWebAPI/api/public/detail?rfId={ESCAPED_rfId_TOKEN}

Store data to the `output/parser-data.txt`

Invoke excel file updater

Repeat cycle...

*/



const Axios = require('axios');
const ExcelJS = require('exceljs');
const PromisePool = require('es6-promise-pool')
const ProxyAgent = require('proxy-agent');
const yargs = require('yargs');
const fs = require('fs');

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function capmonsterCreateTask(capmonsterToken) {
    const data = {
        clientKey: capmonsterToken,
        task: {
            type: 'NoCaptchaTaskProxyless',
            websiteURL: 'https://usr.minjust.gov.ua/content/free-search',
            websiteKey: '6LdStXoUAAAAAE2oEyZLHgu3dBE-WV1zOvZon7_v'
        }
    };

    const response = await axios.post('https://api.capmonster.cloud/createTask', data);

    if (!response.data.errorId) {
        return response.data.taskId;
    } else {
        throw new Error(response.data);
    }
}

async function capmonsterGetTaskResult(capmonsterToken, taskId, retries, delay) {
    retries = retries || 60;
    delay = getRandomArbitrary(3000, 6000);    //delay || 1000;

    const data = {
        "clientKey": capmonsterToken,
        "taskId": taskId
    };

    console.debug('capmonsterGetTaskResult', capmonsterToken, taskId, retries, delay, data);

    let response = null;
    for (let i = 1; i <= retries; i++) {
        response = await axios.post('https://api.capmonster.cloud/getTaskResult/', data);

        console.log('try #' + i);
        console.log('response from https://api.capmonster.cloud/getTaskResult', response.data);

        // without errors
        if (!response.data.errorId) {
            if (response.data.status == 'ready') {
                return response.data.solution.gRecaptchaResponse;
            }

            // if (response.data.status == 'processing') {
            //     await sleep(delay);
            //     continue;
            // }
        } else {
            throw new Error(response.data);
        }

        await sleep(delay);
    }

    // Tries limit reached
    throw new Error('Tries limit reached ' + retries);
}

async function searchEnterpriseRFID(enterpriseType, enterpriseCode, recaptchaToken, proxyAgent) {

    // https://usr.minjust.gov.ua/USRWebAPI/api/public/search?person=2595111406&c=

    const url = 'https://usr.minjust.gov.ua/USRWebAPI/api/public/search?' + enterpriseType + '=' + encodeURIComponent(enterpriseCode) 
                    + '&c=' + encodeURIComponent(recaptchaToken);

    console.log(`searchEnterpriseRFID url = ${url}`);
    
    const response = await axios.get(url, { httpsAgent: proxyAgent });

    const rfId = response.data.searchData[0].rfId;

    if (rfId) {
        return rfId;
    } else {
        console.debug("Can't find enterprise rfId in response: ", response.data);

        throw new Error('Enterprise not found by code #' + enterpriseCode);
    }
}

function prepareApiData(apiData, enterpriseType) {
    const phoneRegex = /(\+?)[\-\(\)\d]{5,}/gm;

    const data = {};

    // Person
    if (enterpriseType == 'person') {
        for (const row of apiData) {
            // phone
            if (row.orderNum == 12) {
                const match = row['value'].match(phoneRegex);
                data['phone'] = match ? match[0] : '-';
            }
        }
    } 
    // Company
    else if (enterpriseType == 'company') {
        for (const row of apiData) {
            // phone
            if (row.orderNum == 30) {
                const match = row['value'].match(phoneRegex);
                data['phone'] = match ? match[0] : '-';
            }
        }
    }

    return data;
}

async function getEnterprisePageData(enterpriseRFID, proxyAgent, enterpriseType) {
    const url = 'https://usr.minjust.gov.ua/USRWebAPI/api/public/detail?rfId=' + encodeURIComponent(enterpriseRFID);

    console.log(`getEnterprisePageData url = ${url}`);

    const response = await axios.get(url, { httpsAgent: proxyAgent });

    console.log('response = ', response.data);

    const data = prepareApiData(response.data, enterpriseType);

    return data;
}

async function getEnterpriseData(capmonsterToken, enterpriseType, enterpriseCode, proxyAgent) {
    const capmonsterTaskId = await capmonsterCreateTask(capmonsterToken);

    // wait 3-6 sec
    // await sleep(getRandomArbitrary(3000, 6000));
    
    console.debug('Created capmonster task with ID #' + capmonsterTaskId);

    const recaptchaToken = await capmonsterGetTaskResult(capmonsterToken, capmonsterTaskId);

    await sleep(1000);

    console.debug('Received ReCaptcha token: ' + recaptchaToken);

    const enterpriseRFID = await searchEnterpriseRFID(enterpriseType, enterpriseCode, recaptchaToken, proxyAgent);

    console.debug('Enterprise rfID: ', enterpriseRFID);

    return await getEnterprisePageData(enterpriseRFID, proxyAgent, enterpriseType);
}

function getProxyForPromise(promiseIndex, proxyList) {
    const proxyTotal = proxyList.length;
    const index = (promiseIndex - Math.trunc(promiseIndex / proxyTotal) * proxyTotal) - 1;

    console.log(`getProxyForPromise; promiseIndex = ${promiseIndex}; proxyTotal = ${proxyTotal}; index = ${index}; value = ${proxyList[index]}`);

    return proxyList[index];
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function getEnterpriseType(inputType) {
    switch (inputType) {
        case 'ф': return 'person';
        case 'ю': return 'company';
        default: return null;
    }
}

// ------------------------------------------------------------------------------------------------------

const argv = yargs(process.argv.slice(2))
    .scriptName('minjust-proxy')
    
    // common options
    .option('capmonster-token', {
        alias: 'token',
        description: 'Capmoster token',
        type: 'string',
    })
    .option('input-filename', {
        alias: 'input',
        description: 'Input excel file name',
        type: 'string',
        default: 'input/list.xlsx',
    })
    .option('output-filename', {
        alias: 'output',
        description: 'Processed excel file name',
        type: 'string',
        default: 'output/updated-list.xlsx',
    })

    // cell options
    .option('cell-code', {
        description: 'Table cell code number',
        type: 'integer',
        default: 3,
    })
    .option('cell-phone', {
        description: 'Table cell phone number',
        type: 'integer',
        default: 8,
    })
    .option('cell-type', {
        description: 'Table cell type number',
        type: 'integer',
        default: 5,
    })

    // threads options
    .option('rows-per-promise', {
        description: 'Processing row count per promise',
        type: 'integer',
        default: 50,
    })
    .option('promise-concurrency', {
        description: 'Maximum promise count in parallel',
        type: 'integer',
        default: 20,
    })

    // excel table processing options
    .option('row-start', {
        description: 'Start from row number',
        type: 'integer',
        default: 0,
    })
    .option('enterprise-types', {
        description: 'Start from row number (support: person, company)',
        type: 'string',
        default: 'person,company',
    })
    .option('worksheet', {
        description: 'Excel worksheet name',
        type: 'string',
        default: 'готово',
    })
    .option('enterprise-type', {
        description: 'Concrete enterprise type. Using when column with type in sheet not defines',
        type: 'string',
    })

    // http client options
    .option('request-timeout', {
        description: 'Http client request timeout',
        type: 'integer',
        default: 60000,
    })
    .option('proxy-list-filename', {
        alias: 'proxy-list',
        description: 'Proxy list file name',
        type: 'string',
        default: 'proxy-list.txt',
    })

    // enterprise types option coerce
    .coerce('enterprise-types', (types) => {
        return types.split(',');
    })

    // proxy list option coerce
    .coerce('proxy-list-filename', (filename) => {
        // console.log(filename, fs.readFileSync(filename, {encoding:'utf8', flag:'r'}).split("\n").filter(line => !!line));
        return fs.readFileSync(filename, {encoding:'utf8', flag:'r'})
                    .split("\n")
                    .filter(line => !!line);
    })

    .help()
    .alias('help', 'h')
    .argv;

// console.log(argv);return;

// common
const capmonsterToken = argv.capmonsterToken;
const inputFilename = argv.inputFilename;
const destFilename = argv.outputFilename;


// Excel
const CELL_CODE = argv.cellCode;
const CELL_PHONE = argv.cellPhone;
const CELL_TYPE = argv.cellType;

// Promise pool configs
let promiseCount = Math.trunc(argv.rowStart / argv.rowsPerPromise);
const rowsPerPromise = argv.rowsPerPromise;
const concurrency = argv.promiseConcurrency;

// Process enterprise types
const processEnterpriseTypes = [
    'person',
    'company',
];

const workbook = new ExcelJS.Workbook();

// Customize http client
const axios = Axios.create();
axios.defaults.timeout = argv.requestTimeout;

const proxyList = argv.proxyList;

async function parse(workbook, rowStart, rowEnd, proxyAgent) {
    const worksheet = workbook.getWorksheet(argv.worksheet);
    const rowsCount = worksheet.actualRowCount;

    rowStart = rowStart || 0;
    rowEnd = rowEnd || rowsCount;

    console.debug(`rowStart = ${rowStart}; rowEnd = ${rowEnd}`);

    for (let rowNum = rowStart; rowNum <= rowEnd; rowNum++) {
        console.debug('Processing row #' + rowNum);

        const row = worksheet.getRow(rowNum);

        let enterpriseCode = row.getCell(CELL_CODE).value;
        const enterpriseType = argv.enterpriseType ?? getEnterpriseType(row.getCell(CELL_TYPE).value);

        // It's not enterprise row or empty
        if (typeof enterpriseCode != 'number' || !enterpriseCode) {
            console.log(`Skipping row #${rowNum} it's empty or not number`);
            continue;
        }

        // // Normalize enterprise code if code length < 8
        // const enterpriseLength = 8;
        // enterpriseCode = enterpriseCode.toString();
        // if (enterpriseCode.length < enterpriseLength) {
        //     enterpriseCode = '0'.repeat(enterpriseLength - enterpriseCode.length) + enterpriseCode;
        //     console.debug('processed code = ' + enterpriseCode);
        // } 
        // // // WARNING: Process only short enterprise codes (optional)
        // // else {
        // //     console.debug(`enterpriseCode.length >= ${enterpriseLength}; code = ${enterpriseCode}`);
        // //     continue;
        // // }

        console.debug(`Row enterprise type is "${enterpriseType}"`);
            
        // Unsupported enterprise type
        if (!enterpriseType) {
            console.error(`Unsupported enterprise type at row #${rowNum}`);
            continue;
        }

        // Process only concret enterprise types
        if (!processEnterpriseTypes.includes(enterpriseType)) {
            console.debug(`Enterprise type "${enterpriseType}" not in: ` + JSON.stringify(processEnterpriseTypes));
            continue;
        }

        // Enterprise telephone already set
        const phone = row.getCell(CELL_PHONE).value;
        if (phone 
                && phone != '-'                                                                             // WARNING: на пол шишки. нужно убрать
                && phone != '#N/A'                                                                          // WARNING: на пол шишки. нужно убрать
                ) {
                    continue;
                }
        if (phone == '0') {                                                                                 // WARNING: на пол шишки. нужно убрать
            console.debug(`Skipping "0" value row`);
            continue;
        }

        console.debug(`Row phone number value = ${phone}`);
                
        // Prepare enterprise code (add start zeros)
        const enterpriseLength = 8;
        enterpriseCode = enterpriseCode.toString();
        if (enterpriseCode.length < enterpriseLength) {
            enterpriseCode = '0'.repeat(enterpriseLength - enterpriseCode.length) + enterpriseCode;
            console.debug('Preapred enterprise code = ' + enterpriseCode);
        }

        console.log('Processing enterprise by code #' + enterpriseCode);

        // continue;

        try {
            const enterpriseData = await getEnterpriseData(capmonsterToken, enterpriseType, enterpriseCode, proxyAgent);

            console.log('Scraped enterprise data:', enterpriseData);

            // updating row columns
            row.getCell(CELL_PHONE).value = enterpriseData.phone;

            // row.getCell(CELL_PHONE).fill = {
            //     type: 'pattern',
            //     pattern:'solid',
            //     fgColor:{argb:'#000000'},
            //     bgColor:{argb:'#FF0000'}
            // };

            // row.commit();

        } catch (e) {
            console.debug('Thrown error when scraping enterprise data', e);
            console.info(`Skiping row #${rowNum}`);

            // set cell value as error
            row.getCell(CELL_PHONE).value = 'error';

            continue;
        }
    }

    // Save backup
    await workbook.xlsx.writeFile(`output/backup-${rowStart}_${rowEnd}.xlsx`);

    // return promiseIndex;
}


// ---------------------------------------------------------------------------------------

/*

    Rinning application

*/


// For first load xlsx file
console.debug('Loading xlsx file: ' + inputFilename);
workbook.xlsx.readFile(inputFilename)
    .then((workbook) => {
        console.debug('Loaded xlsx file: ' + inputFilename);

        const limitPromiseCount = Math.ceil(workbook.getWorksheet(argv.worksheet).actualRowCount / rowsPerPromise);

        console.debug(`limitPromiseCount = ${limitPromiseCount}`);

        const promiseProducer = function () {

            // console.debug(`promiseCount = ${promiseCount}; limitPromiseCount = ${limitPromiseCount}`);

            if (promiseCount < limitPromiseCount) {  // previous value: 1300
                promiseCount++;

                console.log(`promiseCount = ${promiseCount}`);
        
                const rowEnd = (promiseCount * rowsPerPromise) - 1;
                const rowStart = (promiseCount * rowsPerPromise) - rowsPerPromise;
                const promiseIndex = promiseCount;

                // console.debug(`rowEnd = ${rowEnd}; rowStart = ${rowStart}; promiseIndex = ${promiseIndex}`);
                // return new Promise(() => {}, () => {});

                const proxy = getProxyForPromise(promiseIndex, proxyList);
                const proxyAgent = new ProxyAgent(proxy);

                console.log(`Selected proxy for promise: ${proxy}`);
        
                return parse(workbook, rowStart, rowEnd, proxyAgent);
            } else
                return null;
        };

        const pool = new PromisePool(promiseProducer, concurrency);

        pool.addEventListener('fulfilled', function (event) {
            // The event contains:
            // - target:    the PromisePool itself
            // - data:
            //   - promise: the Promise that got fulfilled
            //   - result:  the result of that Promise
            console.log('Fulfilled: ' + event.data.result)
        })
        
        pool.addEventListener('rejected', function (event) {
            // The event contains:
            // - target:    the PromisePool itself
            // - data:
            //   - promise: the Promise that got rejected
            //   - error:   the Error for the rejection
            console.log('Rejected: ' + event.data.error.message)
        })

        pool.start()
            .then(() => {
                console.log('Promise pool finished');
            })
            .finally(() => {
                console.log('Finally');

                workbook.xlsx.writeFile(destFilename);
            });
    });

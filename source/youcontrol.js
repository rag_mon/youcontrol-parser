/*

    input - text file with enterprise code by new line
    output - JSON file with code and scraping data

*/



const casperClass = require('casper');
const utils = require('utils');
var fs = require('fs');

// Casper instance
const casper = casperClass.create({
    pageSettings: {
        loadImages: false,
        loadplugins: false,
        // proxy: '5.252.161.48:8080',
    },
    verbose: true,
    logLevel: 'debug',
    userAgent: 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36'
});

const BASE_URL = 'https://youcontrol.com.ua';
const ENTERPRISE_PAGE_URL = BASE_URL + '/catalog/company_details/{code}/';

const sourceFile = casper.cli.get('source') || 'input/list.txt';
const distFile = casper.cli.get('dist') || 'output/parser-data.txt';

var currentCode = null;
var codes = loadCodeList();

function loadCodeList() {
    // return [
    //     "24583650", 
    //     "36003603", 
    //     "39281096",     // without phone
    // ];
    const codes = [];

    const f = fs.open(sourceFile, 'r');
    do {
        var code = f.readLine();
        
        if (code) codes.push(code);
    } while (code);
    f.close();

    // utils.dump(codes);
    return codes;
}

function getEnterprisePageURL(code) {
    return ENTERPRISE_PAGE_URL.replace('{code}', code);
}

function checkingSiteAvailable(response) {
    if (response == undefined || response.status >= 400)
    {
        this.echo('Site not available');
    } 
    else
    {
        this.echo('Site available');
    }
}

function processEnterprisePageData(response) {
    this.echo('processEnterprisePageData');

    this.waitForSelector('.company-name', function () {
        const data = this.evaluate(getEnterprisePageData);

        storeEnterpriseData.call(this, currentCode, data);
    }, function _onTimeout() {
        this.echo('Error #404', "WARNING");
    }, 2000);
}

function storeEnterpriseData(code, data) {
    this.echo('storeEnterpriseData #' + code);
    utils.dump(data);

    const writeData = JSON.stringify({
        code: data
    }) + "\n";

    fs.write(distFile, writeData, 'a');
}

// -----------------------------------------------------------

/* 
    Browser context
*/

function getEnterprisePageData() {
    const phoneSelector = document.querySelector('.phone-catalog-style');

    return {
        'telephone': phoneSelector ? phoneSelector.innerText : ''
    };
}

// -----------------------------------------------------------

// Error handle
// casper.on('error', function(msg) {
//     this.capture('error.png');
//     this.die(msg);
// });

// Page not found handler
casper.on('http.status.404', function(resource) {
    this.echo('wait, this url is 404: ' + resource.url);
});

// -----------------------------------------------------------

// Checking is site available
casper.start(BASE_URL, function (response) {
    checkingSiteAvailable.call(this, response);
});

// Each enterprise pages 
casper.then(function () {
    this.echo('Each enterprise pages');
    this.echo('Codes');
    utils.dump(codes);

    casper.each(codes, function (self, code) {
        const pageUrl = getEnterprisePageURL(code);
        currentCode = code;

        self.thenOpen(pageUrl, processEnterprisePageData);
    });
});

casper.run();

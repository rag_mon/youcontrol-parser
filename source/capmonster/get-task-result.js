/*

    Create task
    Integration with capmonster.cloud

    > node source/capmonster/get-task-result.js {TASK_ID}
*/

const axios = require('axios');

const capmonsterToken = '258b965dfc46d40502c59f4c3cc08174';
const taskId = process.argv[2];

console.log('taskId = ' + taskId);

const data = {
    "clientKey": capmonsterToken,
    "taskId": taskId
};
const config = {
    responseType: 'json',
};

axios.post('https://api.capmonster.cloud/getTaskResult/', data, config).then((response) => {
    console.log(response.data);
}).catch((error) => {
    console.error(error.response.data);
});



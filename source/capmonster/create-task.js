/*

    Create task
    Integration with capmonster.cloud

    > node source/capmonster/

*/

const axios = require('axios');

const capmonsterToken = '258b965dfc46d40502c59f4c3cc08174'; 

const data = {
    clientKey: capmonsterToken,
    task: {
        type: 'NoCaptchaTaskProxyless',
        websiteURL: 'https://usr.minjust.gov.ua/content/free-search',
        websiteKey: '6LdStXoUAAAAAE2oEyZLHgu3dBE-WV1zOvZon7_v'
    }
};
const config = {
    responseType: 'json',
};

axios.post('https://api.capmonster.cloud/createTask', data, config).then((response) => {
    console.log(response.data);
}).catch((error) => {
    console.error(error.response.data);
});


